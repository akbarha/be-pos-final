const Inventories = require('../models/Inventories')
const Cart = require('../models/cartModels')

class CartController {
    static getCart = async(req,res) => {
        const cartid = req.cookies.cartid
        try {
            if(!cartid){
                res.json({info:`Tidak ada Cart`})
            }
            else {
                const cart = await Cart.findById(cartid).populate({
                    path:"items.productId"
                })
                res.json(cart)
            }
        }
        catch(err){
            res.status(500).json(err)
        }

    }

    // tambah item harus selalu lewat ini
    static addItemToCart = async(req,res)=>{
        const cartid = req.cookies.cartid // id cart untuk setiap pengguna yang menggunakan aplikasi

        // req.body data
        const quantity = Number.parseInt(req.body.quantity)
        const productId = req.body.productId

        // 
        let cart = null

        try {
            // kalau belum ada cart yang dibuat dalam session untuk user spesifik ini maka :
            if(!cartid){
                cart = await Cart.create({})
                // save id cart ke cookie
                res.cookie('cartid', cart._id, {httpOnly:true})
            }

            // kalau cartid udah ada berarti udah ada cart.
            else{
                cart = await Cart.findById(cartid)
            }
            
            // cari produk dulu dari _id bawaan
            const productDetails = await Inventories.findById(productId)

            // kalau produk yang dicari kagak ada
            if(!productDetails){
                return res.status(404).json({info:'Product Not Found'})
            }

            // cek apakah request pembelian qty produk melebih stok
            if(quantity > productDetails.goods_stock){
                let data = await Cart.findById(cart._id).populate({
                    path:"items.productId",
                    select:'goods_name selling_price'

                })
                return res.status(500).json({info:'Barang Tidak Cukup', data})
            }

            productDetails.goods_stock = productDetails.goods_stock - quantity
            await productDetails.save()

            // cek apa item sudah ada di dalam cart atau belum
            const indexFound = cart.items.findIndex(item => item.productId._id == productId)

            if(indexFound !== -1 && quantity<=0){
                cart.items.splice(indexFound,1)
                if(cart.items.length == 0){
                    cart.subTotal = 0
                }
                else{
                    cart.subTotal = cart.items.map(item=>item.total).reduce((acc,next)=>acc+next)
                }
            }

            // kalau ada tambah aja ke yang udah
            else if(indexFound!== -1){
                cart.items[indexFound].quantity = cart.items[indexFound].quantity + quantity
                cart.items[indexFound].total = cart.items[indexFound].quantity * productDetails.selling_price
                cart.items[indexFound].price = productDetails.selling_price
                cart.subTotal = cart.items.map(item => item.total).reduce((acc,next) => acc + next)
            }

            // kalau engga ada  push soalnya itu item baru
            else if(quantity>0){
                cart.items.push({
                    productId:productId,
                    quantity:quantity,
                    price:productDetails.selling_price,
                    total:parseInt(productDetails.selling_price * quantity)
                })
                cart.subTotal = cart.items.map(item=>item.total).reduce((acc,next) => acc + next)
            }

            // kalau qty 0 atau dibawahnya invalid requrest
            else {
                return res.status(400).json({
                    info:"Invalid Request"
                })
            }

            await cart.save()
            let data = await Cart.findById(cart._id).populate({
                path:"items.productId",
                select:'goods_name selling_price'
            })

            res.json(data)
        }

        // kalau error
        catch(err){
            console.error(err)
            res.status(500).json(err)
        }
    }

    // kurangi item harus lewat sini
    static updateCart = async(req,res)=>{
        const cartid = req.cookies.cartid;
        const {productId, quantity} = req.body

        if(!cartid){
            res.status(404).json({info:'Anda tidak memiliki Cart'})
        }

        try {
            // cari cart
            const cart = await Cart.findById(cartid)
            if(!cart){
                return res.status(404).json({info : 'Cart Tidak Ditemukan'})
            }

            const productDetails = await Inventories.findById(productId)
            if(!productDetails) {
                return res.status(404).json({info:'Product Tidak Ditemukan'})
            }


            // cari item yang akan diubah dalam cart
            const indexFound = cart.items.findIndex(item => item.productId == productId)

            if (quantity > cart.items[indexFound].quantity){
                return res.status(401).json({info:'Jika ingin menambahkan item ke cart harus menggunakan rute addItemToCart'})
            }

            if(indexFound !== -1) {
                productDetails.goods_stock = productDetails.goods_stock + (cart.items[indexFound].quantity - quantity)
                cart.items[indexFound].quantity = quantity;
                cart.items[indexFound].total = quantity * productDetails.selling_price
                cart.subTotal = cart.items.map(item=>item.total).reduce((acc, next) => acc + next)
                await cart.save()

                let data = await Cart.findById(cartid).populate({
                    path:'items.productId',
                    select:'goods_name, selling_price'
                })

                return res.json({data})
            }
            else {
                return res.status(404).json({info:'Barang tersebut tidak ada di dalam cart'})
            }
        }
        catch(err){
            console.error(err)
            res.status(500).json(err)
        }
    }

    static deleteItemInCart = async(req,res) => {
        // hapus item tertentu dari dalam cart
        const cartid = req.cookies.cartid
        const {productId} = req.body

        if(!cartid){
            res.status(404).json({info:'Anda tidak memiliki cart'})
        }

        try {
            let cart = await Cart.findById(cartid)
            if(!cart){
                res.status(404).json({info:'Cart tidak ditemukan'})
            }
            // cari item di kart
            const indexFound = cart.items.findIndex(item => item.productId == productId)
            if(indexFound !== -1){
                // remove item dari cart
                cart.items.splice(indexFound,1)
                
                // jika dalam cart sudah tidak ada item lagi
                if(cart.items.length === 0){
                    cart.subTotal = 0;
                }
                // jika tidak
                else {
                    cart.subTotal = cart.items.map(item => item.total).reduce((acc,next)=> acc+next)
                }

                await cart.save()
                let data = await Cart.findById(cartid).populate({
                    path:'items.productId',
                    select:'goods_name selling_price'
                })
                res.json(data)
            }

            else {
                return res.status(404).json({info:'Barang tidak ada dalam cart!'})
            }
        }
        catch(err){
            console.error(err)
            res.status(500).json(err)
        }
    }

    static resetCart = async(req,res) => {

        // push ke transaction history --> pindah ke transactionhistory controller

        // ini cuman buat reset dan destroy cart (hapus cart dari db, hapus cartid dari cookies)
        // kalau transaksi berhasil pake controller TransactionHistory

    }
}

module.exports = CartController