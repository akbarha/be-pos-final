const { get } = require('mongoose')
const Users = require('../models/Users')
const Role = require('../models/Role')
const bcrypt = require ('bcrypt')
const jwt = require('jsonwebtoken')
require("dotenv").config()
const {generateRandomString} = require('../helper/generate')
const {generateAccessToken, generateRefreshToken} = require('../middlewares/Auth')

class Controller {
    static userRegister = async (req, res, next) => {
        let {username, email, role_id} = req.body
        let password = await bcrypt.hash(req.body.password, 10)
        let user_id = `user-${generateRandomString(4)}-${Date.now().toString(36)}`
        try{
            let checkUserEmail = await Users.find({$or:[{username: username}, {email:email}]})
            if(checkUserEmail.length > 0){
                res.json("Username atau email sudah terpakai")
            }
            else{
                let role = await Role.update({role_id: role_id},{$push:{listUser: {user_id}}})
                res.json(role)
            //     let newUser = await Users.create({user_id, username, email, password, role_id, update_at: Date.now()})
            //     const encryptUser = {
            //         user_id: newUser.user_id,
            //         username: newUser.username,
            //         role_id: newUser.role_id
            //     }
            //     res.json(generateAccessToken(encryptUser))
            }
        }catch(err){
            res.status(500).json({err})
        }
    }
    
    static getUser = async (req, res) => {
        let{user_id, username, email} = req.userData
        try{
            let allData = await Users.find({user_id: user_id})
            res.json(allData)
        }catch(err){
            res.status(500).json({err})
        }
    }

    static getAllUser = async (req, res) => {
        try{
            let allData = await Users.find()
            res.json(allData)
        }catch(err){
            res.status(500).json({err})
        }
    }

    static updateUser = async (req, res) => {
        let {user_id, username, email} = req.userData
        let {newUsername, newEmail} = req.body
        let getData = await Users.find({user_id: user_id})
        try{
            if(await Users.find({username: newUsername}) != ""){
                res.json("Username sudah terpakai")
            }
            else if(await Users.find({email: newEmail}) != ""){
                res.json("Email sudah terpakai")
            }
            else{
                getData[0].username = newUsername
                getData[0].email = newEmail
                getData[0].update_at = Date.now()
                await getData[0].save()
                res.json({getData, info: "Data sukses diupdate"})
            }
        }catch(err){
            res.status(500).json({err})
        }
    }

    static deleteUser = async (req, res) => {
        let{user_id} = req.userData.encryptUser
        try{
            let getUser = await Users.find({user_id: user_id})
            getUser[0].softDelete = true
            await getUser[0].save()
            res.json({getUser, info: "Success Deleted"})
        }catch(err){
            res.status(500).json({err})
        }
    }

    static loginUser = async (req, res) => {
        let {username, email, password} = req.body
        let getData = await Users.find({$or: [{username: username}, {email: email}] })
        if(getData != "" && !getData[0].softDelete){
            if(await bcrypt.compare(password, getData[0].password)){
                const encryptUser = {
                    _id: getData[0]._id,
                    user_id: getData[0].user_id,
                    username: getData[0].username,
                    role_id: getData[0].role_id
                }
                const authToken = generateAccessToken(encryptUser)
                const refreshToken = generateRefreshToken(encryptUser)
                res.json({authToken,refreshToken})
            }
            else{
                res.json("Password Salah")
            }
        }
        else{
            res.status(404).json("User Not Found")
        }
    }
}

module.exports = Controller