const {generateRandomString} = require('../helper/generate')
const Inventories = require('../models/Inventories')

class InventoriesController {
    static getAllInvetories = async(req,res)=>{
        try {
            const allInvetories = await Inventories.find({})
            res.json(allInvetories)
        }
        catch(err){
            res.status(500).json({err})
        }
    }

    static getInventoryById = async(req,res)=>{
        const {id} = req.params
        try {
            const inventory = await Inventories.findById(id)
            res.json(inventory)
        }
        catch(err){
            res.status(500).json({err})
        }
    }

    static createInventory = async(req,res)=>{
        const random_id = generateRandomString(4)
        const inventory_id = `category-id-${random_id}-${Date.now().toString(36)}`
        try {
            const {goods_name,product_category_id,purchase_price,selling_price,goods_stock} = req.body
            const newInventory = await Inventories.create({
                inventory_id,goods_name,product_category_id,purchase_price,selling_price,goods_stock
            })
            res.json({newInventory, info:'New Inventory Successfully Created'})
        }
        catch(err){
            res.status(500).json({err})
        }
    }

    static updateInventory = async(req,res) => {
        const {id} = req.params
        try {
            const inventory = await Inventories.findById(id)
            if(!inventory){
                res.status(404).json({message:'Inventory not found!'})
            }

            const {goods_name,product_category_id,purchase_price,selling_price,goods_stock} = req.body
            inventory.goods_name = goods_name || inventory.goods_name
            inventory.product_category_id = product_category_id || inventory.product_category_id
            inventory.purchase_price = purchase_price || inventory.purchase_price
            inventory.selling_price = selling_price || inventory.selling_price
            inventory.goods_stock = goods_stock || inventory.goods_stock
            await inventory.save()
            res.json({inventory, info:'Inventory Successfully Updated'})
        }
        catch(err){
            res.status(500).json({err})
        }
    }

    static deleteInventory = async(req,res)=>{
        const {id} = req.params
        try{
            const deletedInventory = await Inventories.findByIdAndDelete(id)
            if(deletedInventory){
                res.json({info:'Inventory Successfully Deleted'})
            }
            else{
                res.status(404).json({info:'Inventory Not Found!'})
            }
        }
        catch(err){
            res.status(500).json({err})
        }
    }
}

module.exports = InventoriesController