const TransactionHistory = require('../models/TransactionHistory')
const Cart = require('../models/cartModels')

class TransactionHistoryController {
    static completeTransaction = async(req,res) => {
        try {
            const userId = req.userData._id
            const cartid = req.cookies.cartid
            if(!userId){
                return res.status(401).json({info:'Unauthorized'})
            }

            if(!cartid){
                return res.status(404).json({info:'User ini tidak ada cart yang aktif'})
            }

            const cart = await Cart.findById(cartid).populate({
                path:'items.productId',
                select:'goods_name selling_price'
            })

            if(!cart){
                return res.status(404).json({info:'Tidak ada Cart yang aktif'})
            }

            // buat data transaction
            const transaction =  {
                items:cart.items,
                subTotal:cart.subTotal,
                createdAt:Date.now()
            }

            // save data transaction
            await TransactionHistory.create({
                user:userId,
                transaction:transaction
            })

            // delete currentuser cart
            await Cart.findByIdAndDelete(cartid)
            res.clearCookie('cartid')
            res.json({info:'Transaction selesai dan tertambah ke history'})
        }
        catch(err){
            console.error(err)
            res.status(500).json(err)
        }
    }

    static getTransaction = async(req,res)=>{
        try {
            const transactionData = await TransactionHistory.find({}).populate()
            res.json(transactionData)
        }
        catch(err){
            res.status(500).json(err)
        }    
    }

    // dev use only
    static clearCookie = async(req,res)=>{
        res.clearCookie('cartid')
        res.json('Cookie Cleared')
    }
}

module.exports = TransactionHistoryController