const Category = require('../models/ProductCategory')
const {generateRandomString} = require('../helper/generate')

class CategoryController {
    static getAllCategory = async (req,res) =>{
        try {
            const allCategory = await Category.find({})
            res.json(allCategory)
        }
        catch(err){
            res.status(500).json({err})
        }
    }

    static createCategory = async(req,res) => {
        try {
            let random_id = generateRandomString(4)
            const product_category_id = `category-id-${random_id}-${Date.now().toString(36)}`
            const {product_category} = req.body
            const new_category = await Category.create({
                product_category_id,
                product_category
            })
            res.json({new_category, info:`New Category successfully created`})
        }
        catch(err){
            res.status(500).json({err})
        }

    }

    static deleteCategory = async(req,res)=>{
        const {id} = req.params
        try {
            const category = Category.findById(id)
            if(!category){
                return res.status(404).json({error:"Category not found"})
            }

            await category.deleteOne()
            res.json({info:'Successfully Deleted'})
        }
        catch(err){
            res.status(500).json({err})
        }
    }
}

module.exports = CategoryController