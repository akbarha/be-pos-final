const { get } = require('mongoose')
const Role = require('../models/Role')
require("dotenv").config()
const {generateRandomString} = require('../helper/generate')

class Controller {
    static addRole = async (req, res) => {
        let {roleName} = req.body
        let role_id = `role-${generateRandomString(4)}-${Date.now().toString(36)}`
        try{
            let role = await Role.create({role_id, roleName, update_at: Date.now()})
            res.json({role,info:'success'})
        }catch(err){
            res.status(500).json({err})
        }
    }

    static getAllRole = async (req, res) => {
        try{
            let allRole = await Role.find({softDelete: false})
            res.json(allRole)
        }catch(err){
            res.status(500).json({err})
        }
    }

    static updateRole = async (req, res) => {
        let {role_id} = req.params
        let {newRoleName} = req.body
        let getRole = await Role.find({$and:[{softDelete: false},{role_id: role_id}]})
        try{
            if(await Role.find({roleName: newRoleName}) != ""){
                res.json("Role sudah dibuat")
            }
            else{
                getRole[0].roleName = newRoleName
                getRole[0].update_at = Date.now()
                await getRole[0].save()
                res.json({getRole, info: "Role sukses diupdate"})
            }
        }catch(err){
            res.status(500).json({err})
        }
    }

    static deleteRole = async (req, res) => {
        let{role_id} = req.query
        try{
            let getRole = await Role.find({$and:[{softDelete: false},{role_id: role_id}]})
            getRole[0].softDelete = true
            await getRole[0].save()
            res.json({getRole, info: "Success Deleted"})
        }catch(err){
            res.status(500).json({err})
        }
    }
}

module.exports = Controller