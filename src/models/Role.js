const mongoose = require("mongoose")
, Schema = mongoose.Schema

const schemaRole = new Schema({
  role_id: {type: String, require: true},
  roleName: {type: String, require:true},
  listUser: {type: Array, default: []},
  softDelete:{type: Boolean, default: false},
  create_at: {type: Date, default: Date.now},
  update_at: Date
})

const Role = mongoose.model('Role', schemaRole)

module.exports = Role
