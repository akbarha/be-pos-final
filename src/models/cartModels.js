const mongoose = require('mongoose')
const ItemSchema = new mongoose.Schema({
    productId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Inventories'
    },
    quantity:{
        type:Number,
        required:true,
        min:[1, 'Qty cannot be less than 1']
    },
    price:{
        type:Number,
        required:true,
    },
    total:{
        type:Number,
        required:true
    }
})

const CartSchema = new mongoose.Schema({
    items:[ItemSchema],
    subTotal:{
        default:0,
        type:Number
    }
})

const Cart = mongoose.model('Cart', CartSchema)
module.exports = Cart