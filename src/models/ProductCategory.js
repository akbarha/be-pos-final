const mongoose = require("mongoose")
, Schema = mongoose.Schema

const schemaProductCategory = new Schema({
  product_category_id: {type: String, required: []},
  product_category: {type: String, required: true},
  create_at: {type: Date, default: Date.now},
  update_at: Date
})

const ProductCategory = mongoose.model('ProductCategory', schemaProductCategory)

module.exports = ProductCategory