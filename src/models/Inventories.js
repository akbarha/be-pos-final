const ProductCategory = require("./ProductCategory")

const mongoose = require("mongoose")
, Schema = mongoose.Schema

const schemaInventory = new Schema({
  inventory_id: {type: String, require:[]},
  goods_name: {type: String, required: true},
  product_category_id: {type: String, required: true},
  purchase_price: {type: Number, require:true},
  selling_price: {type: Number, require:true},
  goods_stock: {type: Number, require: true},
  create_at: {type: Date, default: Date.now},
  update_at: Date
})

const Inventories = mongoose.model('Inventories', schemaInventory)

module.exports = Inventories