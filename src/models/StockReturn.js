const mongoose = require("mongoose")
, Schema = mongoose.Schema

const schemaStockReturn = new Schema({
  stock_return_id: {type: String, require:[]},
  transaction_id: {type: String, require:[]},
  inventory_id: {type: String, require:[]},
  return_quantity: {type: Number, require: true},
  reason: {type: String, require: true},
  create_at: {type: Date, default: Date.now},
  update_at: Date
})

const StockReturn = mongoose.model('StockReturn', schemaStockReturn)

module.exports = StockReturn