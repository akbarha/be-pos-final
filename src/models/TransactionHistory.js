const mongoose = require("mongoose")
, Schema = mongoose.Schema

// const schemaTransactionList = new Schema({
//   transaction_id: {type: String, require:[]},
//   inventory_id: {type: String, require:[]},
//   product_id:{type:String, required:true},
//   qty_goods: {type: Number, required: true},
//   total_price: {type: Number, required: true},
//   create_at: {type: Date, default: Date.now},
//   update_at: Date
// })

// const TransactionList = mongoose.model('TransactionList', schemaTransactionList)
// module.exports = TransactionList

const TransactionItemSchema = new mongoose.Schema({
  productId:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'Inventories'
  },

  quantity:{
    type:Number,
    required:true,
    min:[1, 'Qty cannot be less than 1']
  },

  price:{
    type:Number,
    required:true
  },

  total:{
    type:Number,
    required:true
  }

})

const TransactionSchema = new mongoose.Schema({
  items:[TransactionItemSchema],
  subTotal:{
    default:0,
    type:Number
  },
  createdAt:{
    type:Date,
    default:Date.now
  }
})

const TransactionHistorySchema = new mongoose.Schema({
  transaction:TransactionSchema,
  user:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  }
})

const TransactionHistory = mongoose.model('TransactionHistory', TransactionHistorySchema)
module.exports = TransactionHistory