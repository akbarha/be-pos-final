const mongoose = require("mongoose")
, Schema = mongoose.Schema

const schemaUsers = new Schema({
  user_id: {type: String, require: true},
  username: {type: String, required: true},
  email: {type: String, required: true},
  password: {type: String, required: true},
  role_id: {type: String, require: true},
  softDelete:{type: Boolean, default: false},
  create_at: {type: Date, default: Date.now},
  update_at: Date
})

const Users = mongoose.model('Users', schemaUsers)

module.exports = Users
