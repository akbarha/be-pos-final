const mongoose = require("mongoose")
, Schema = mongoose.Schema

const schemaStock = new Schema({
  stock_id: {type: String, require:[]},
  vendor_name: {type: String, require: true},
  create_at: {type: Date, default: Date.now},
  update_at: Date
})

const Stock = mongoose.model('Stock', schemaStock)

module.exports = Stock