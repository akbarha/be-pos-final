const mongoose = require("mongoose")
, Schema = mongoose.Schema

const schemaCashier = new Schema({
  cashier_id: {type: String, require:[]},
  transaction_id: {type: String, require:[]},
  create_at: {type: Date, default: Date.now},
  update_at: Date
})

const Cashier = mongoose.model('Cashier', schemaCashier)

module.exports = Cashier