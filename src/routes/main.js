const express = require('express')
const UserController = require("../controllers/usersController")
const CategoryController = require('../controllers/categoryController')
const RoleController = require('../controllers/roleController')
const {verifyToken, extractJWT} = require("../middlewares/Auth")
const InventoriesController = require('../controllers/inventoryController')
const CartController = require('../controllers/cartController')
const TransactionHistory = require('../models/TransactionHistory')
const TransactionHistoryController = require('../controllers/TransactionHistoryController')

const router = express.Router()

// User 
router.post('/user', UserController.userRegister)
router.get('/user', verifyToken, extractJWT, UserController.getUser)
router.get('/allUser', verifyToken, extractJWT, UserController.getAllUser)
router.patch('/user', verifyToken, extractJWT, UserController.updateUser)
router.delete('/user/:user_id', verifyToken, extractJWT, UserController.deleteUser)
router.post('/login', UserController.loginUser)

// Role
router.post('/role', RoleController.addRole)
router.get('/role', RoleController.getAllRole)
router.patch('/role/:role_id', RoleController.updateRole)
router.delete('/role', RoleController.deleteRole)

// Category
router.get('/category', CategoryController.getAllCategory)
router.post('/category', verifyToken, extractJWT, CategoryController.createCategory)
router.delete('/category/:id', verifyToken, extractJWT, CategoryController.deleteCategory)

// Inventory
router.post('/inventory', verifyToken, extractJWT, InventoriesController.createInventory)
router.put('/inventory/:id', verifyToken, extractJWT,InventoriesController.updateInventory)
router.get('/inventory', InventoriesController.getAllInvetories)
router.get('/inventory/:id', InventoriesController.getInventoryById)
router.delete('/inventory/:id', verifyToken, extractJWT, InventoriesController.deleteInventory)

// Cart
// getCart (harus sudah create cart sebelumnya via addItemToCart)
router.get('/cart', CartController.getCart)
// tambah item ke cart harus pake ini
router.post('/cart', verifyToken, extractJWT, CartController.addItemToCart)
// update item cart (mengurangi item ke sini) (harus udah create cart sebelumnya via addItemToCart)
router.put('/cart', verifyToken, extractJWT, CartController.updateCart)
// delete item cart (menghapus item sampe 0 dari cart ke sini) (harus udah create cart sebelumnya via addItemToCart)
router.delete('/cart', verifyToken, extractJWT, CartController.deleteItemInCart)

// Transaction History
/* kalau transaksi berhasil, push cart ke Transaction History + Destroy Cart yang ada sebelumnya. bisa lewat sini */
router.post('/transaction',verifyToken, extractJWT, TransactionHistoryController.completeTransaction)
router.get('/transaction', TransactionHistoryController.getTransaction)
// dev use only
router.get('/clearcookie', TransactionHistoryController.clearCookie)

// Root Page
router.get('/', (req,res)=>{
    res.json({info: "this is root page"})
})



module.exports = router