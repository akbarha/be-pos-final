const jwt = require("jsonwebtoken")
require("dotenv").config()

// accessTokens
function generateAccessToken(user) {
    return jwt.sign(user, process.env.AUTH_SECRET_KEY, {expiresIn: "15m"}) 
}

// refreshTokens
function generateRefreshToken(user) {
    return jwt.sign(user, process.env.REFRESH_TOKEN_KEY, {expiresIn: "20m"})
}

const verifyToken = (req,res,next) => {
    const bearerHeader = req.headers['authorization']
    if(typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(' ')
        const bearerToken = bearer[1]
        req.token = bearerToken
        next();
    }else{
        res.sendStatus(403)
    }
}

const extractJWT = (req,res,next) => {
    jwt.verify(req.token,process.env.AUTH_SECRET_KEY,(err,authData)=>{
        if(err){
            res.sendStatus(403)
        }else{
            req.userData = authData
            next();
        }
    })
}

module.exports = {
    generateAccessToken,
    generateRefreshToken,
    verifyToken,
    extractJWT
}