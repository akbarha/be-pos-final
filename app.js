require("dotenv").config()
const cookieParser = require('cookie-parser')
const cors = require('cors')

const express = require('express')
const mongoose =require('mongoose')

const router = require("./src/routes/main")

const app = express()
const port = process.env.TOKEN_SERVER_PORT

const uri ="mongodb+srv://nayupryandoko:k9UYhLcwhVdUZSH5@cluster0.5h79oqo.mongodb.net/?retryWrites=true&w=majority"

mongoose.connect(uri).then(()=>{
  console.log("Connected to MongoDB");
})
.catch(()=>{
  console.log("Couldn't connect to MongoDB");
})

app.use(cookieParser())
app.use(express.json())
app.use(cors())
app.use("/", router)

app.listen(port, () => {
  console.log(`BE Final Project listening on port ${port}`)
})

module.exports = app